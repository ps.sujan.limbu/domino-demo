package com.sujan.mydominotestproject.client.ui.application.content.screen02;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import com.sujan.mydominotestproject.shared.model.Gender;
import com.sujan.mydominotestproject.shared.model.MyModel;
import com.sujan.mydominotestproject.shared.model.User;
import elemental2.dom.HTMLElement;
import java.lang.Override;
import java.util.ArrayList;
import java.util.List;

import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.datatable.ColumnConfig;
import org.dominokit.domino.ui.datatable.DataTable;
import org.dominokit.domino.ui.datatable.DataTableStyles;
import org.dominokit.domino.ui.datatable.TableConfig;
import org.dominokit.domino.ui.datatable.store.LocalListDataStore;
import org.dominokit.domino.ui.utils.TextNode;

import static com.sujan.mydominotestproject.shared.model.Gender.*;

/**
 * Copyright (C) 2018 - 2019 Frank Hossfeld <frank.hossfeld@googlemail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Screen02Component extends AbstractComponent<IScreen02Component.Controller, HTMLElement> implements IScreen02Component {
  private Card card;
  private TableConfig<User> tableConfig;
    private List<UserListParseHandler> userListParseHandlers = new ArrayList<>();

  public Screen02Component() {
    super();
  }

  @Override
  public void edit(MyModel model) {
    // That's a good place to move your data out of the model into the widgets.
    // 
    // Using GWT 2.x you can use the editor framework and in this case
    // it is a good idea to edit and flush the data inside the presenter.
//    card.setTitle(model.getActiveScreen());
  }

  @Override
  public void render() {
        tableConfig = new TableConfig<>();
        tableConfig.addColumn(ColumnConfig.<User>create("id","#")
                .asHeader()
                .setCellRenderer(
                cellInfo -> TextNode.of(cellInfo.getTableRow().getRecord().getUsername())
                ))
                .addColumn(ColumnConfig.<User>create("username","Username")
                        .asHeader()
                        .setCellRenderer(
                        cellInfo -> TextNode.of(cellInfo.getTableRow().getRecord().getUsername())
                ))
                .addColumn(ColumnConfig.<User>create("email","Email")
                        .setCellRenderer(
                                cellInfo -> TextNode.of(cellInfo.getTableRow().getRecord().getEmail())
                        ))
                .addColumn(ColumnConfig.<User>create("gender","Gender")
                        .setCellRenderer(
                                cellInfo -> TextNode.of(cellInfo.getTableRow().getRecord().getGender().name())
                ))
                .addColumn(ColumnConfig.<User>create("verification","Verified")
                        .setCellRenderer(
                                cellInfo -> TextNode.of(cellInfo.getTableRow().getRecord().getVerification().toString())
                ));

        LocalListDataStore<User> localListDataStore = new LocalListDataStore<>();
        localListDataStore.setData(getUsers());
        DataTable<User> table = new DataTable<>(tableConfig, localListDataStore);

        card = Card.create("Users","A sample of basic table")
                .setCollapsible()
                .appendChild(table);

        table.load();
        initElement(card.element());
  }

  private List<User> getUsers(){
      List<User> users = new ArrayList<>();
      users.add(new User("1", "cap", MALE,"captainamerica@avenger.com", true));
      users.add(new User("2", "ironman", MALE,"ironman@avenger.com", true));
      users.add(new User("3", "widow", FEMALE,"black.widow@avenger.com", true));
      users.add(new User("4", "captainMarvel", FEMALE,"captain_marvel@avenger.com", true));
      users.add(new User("5", "thor", MALE,"thor.odison@avenger.com", true));
      users.add(new User("6", "thehulk", MALE,"hulk.banner@avenger.com", true));
      users.add(new User("7", "hawkeye", MALE,"hawk.i@avenger.com", true));
      users.add(new User("8", "antman", MALE,"antman@avenger.com", true));
      users.add(new User("9", "waps", FEMALE,"wapsy@avenger.com", true));
      users.add(new User("10", "strange", MALE,"doc.strange@avenger.com", true));
      users.add(new User("11", "spiderman", MALE,"spidy@avenger.com", true));
      users.add(new User("12", "shangchi", MALE,"shang.chi@avenger.com", false));
      users.add(new User("13", "venom", MALE,"venom@avenger.com", false));
      return users;
  }

    public interface UserListParseHandler {
        void onUsersParsed(List<User> contacts);
    }
}
