package com.sujan.mydominotestproject.client.ui.application.content.screen01;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import com.sujan.mydominotestproject.shared.model.MyModel;
import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLElement;
import java.lang.Override;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.utils.TextNode;

/**
 * Copyright (C) 2018 - 2019 Frank Hossfeld <frank.hossfeld@googlemail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Screen01Component extends AbstractComponent<IScreen01Component.Controller, HTMLElement> implements IScreen01Component {
  private Card card;

  public Screen01Component() {
    super();
  }

  @Override
  public void edit(MyModel model) {
    // That's a good place to move your data out of the model into the widgets.
    // 
    // Using GWT 2.x you can use the editor framework and in this case
    // it is a good idea to edit and flush the data inside the presenter.
//    card.setTitle(model.getActiveScreen());
  }

  @Override
  public void render() {
    card = Card.create("");
    initElement(card.element());
    card.appendChild(
            Row.create()
                    .addColumn(
                            Column.span4()
                                    .appendChild(
                                            Card.create("Light Blue Card", "Description text here...")
                                                    .setBackground(Color.LIGHT_BLUE)
                                                    .appendChild(TextNode.of("Something over here"))
                                                    .addHeaderAction(
                                                            Icons.ALL.more_vert(),
                                                            event -> DomGlobal.console.info("More action selected"))))
    );
  }
}
