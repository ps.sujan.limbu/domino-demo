package com.sujan.mydominotestproject.client.ui.application.shell;

import com.github.nalukit.nalu.client.Router;
import com.github.nalukit.nalu.client.exception.RoutingInterceptionException;
import com.github.nalukit.nalu.client.internal.AbstractShellCreator;
import com.github.nalukit.nalu.client.internal.ClientLogger;
import com.github.nalukit.nalu.client.internal.application.IsShellCreator;
import com.github.nalukit.nalu.client.internal.application.ShellInstance;
import com.sujan.mydominotestproject.client.MyDominoTestProjectContext;
import java.lang.Object;
import java.lang.Override;
import java.lang.StringBuilder;
import org.gwtproject.event.shared.SimpleEventBus;

/**
 * Build with Nalu version >>2.0.1<< at >>2021.11.11-14:51:26<<
 */
public final class ApplicationShellCreatorImpl extends AbstractShellCreator<MyDominoTestProjectContext> implements IsShellCreator {
  public ApplicationShellCreatorImpl(Router router, MyDominoTestProjectContext context,
      SimpleEventBus eventBus) {
    super(router, context, eventBus);
  }

  @Override
  public ShellInstance create() {
    StringBuilder sb01 = new StringBuilder();
    ShellInstance shellInstance = new ShellInstance();
    shellInstance.setShellClassName("com.sujan.mydominotestproject.client.ui.application.shell.ApplicationShell");
    sb01.append("shell >>com.sujan.mydominotestproject.client.ui.application.shell.ApplicationShell<< --> will be created");
    ClientLogger.get().logSimple(sb01.toString(), 1);
    ApplicationShell shell = new ApplicationShell();
    shellInstance.setShell(shell);
    shell.setContext(context);
    shell.setEventBus(eventBus);
    shell.setRouter(router);
    sb01.setLength(0);
    sb01.append("shell >>com.sujan.mydominotestproject.client.ui.application.shell.ApplicationShell<< --> created and data injected");
    ClientLogger.get().logDetailed(sb01.toString(), 2);
    sb01.setLength(0);
    return shellInstance;
  }

  @Override
  public void onFinishCreating(Object object) throws RoutingInterceptionException {
    ApplicationShell shell = (ApplicationShell) object;
  }
}
