package com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar;

import com.github.nalukit.nalu.client.Router;
import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.exception.RoutingInterceptionException;
import com.github.nalukit.nalu.client.internal.AbstractControllerCreator;
import com.github.nalukit.nalu.client.internal.ClientLogger;
import com.github.nalukit.nalu.client.internal.application.ControllerFactory;
import com.github.nalukit.nalu.client.internal.application.ControllerInstance;
import com.github.nalukit.nalu.client.internal.application.IsControllerCreator;
import com.sujan.mydominotestproject.client.MyDominoTestProjectContext;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.StringBuilder;
import org.gwtproject.event.shared.SimpleEventBus;

/**
 * Build with Nalu version >>2.0.1<< at >>2021.11.11-14:51:26<<
 */
public final class StatusbarControllerCreatorImpl extends AbstractControllerCreator<MyDominoTestProjectContext> implements IsControllerCreator {
  public StatusbarControllerCreatorImpl(Router router, MyDominoTestProjectContext context,
      SimpleEventBus eventBus) {
    super(router, context, eventBus);
  }

  @Override
  public ControllerInstance create() {
    StringBuilder sb01 = new StringBuilder();
    ControllerInstance controllerInstance = new ControllerInstance();
    controllerInstance.setControllerClassName("com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar.StatusbarController");
    AbstractComponentController<?, ?, ?> storedController = ControllerFactory.get().getControllerFormStore("com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar.StatusbarController");
    if (storedController == null) {
      sb01.append("controller >>com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar.StatusbarController<< --> will be created");
      ClientLogger.get().logSimple(sb01.toString(), 3);
      StatusbarController controller = new StatusbarController();
      controllerInstance.setController(controller);
      controllerInstance.setCached(false);
      controller.setContext(context);
      controller.setEventBus(eventBus);
      controller.setRouter(router);
      controller.setCached(false);
      controller.setRelatedRoute("/application");
      controller.setRelatedSelector("footer");
      sb01.setLength(0);
      sb01.append("controller >>").append(controller.getClass().getCanonicalName()).append("<< --> created and data injected");
      ClientLogger.get().logDetailed(sb01.toString(), 4);
    } else {
      sb01.append("controller >>").append(storedController.getClass().getCanonicalName()).append("<< --> found in cache -> REUSE!");
      ClientLogger.get().logDetailed(sb01.toString(), 4);
      controllerInstance.setController(storedController);
      controllerInstance.setCached(true);
      controllerInstance.getController().setCached(true);
    }
    return controllerInstance;
  }

  @Override
  public void onFinishCreating(Object object) throws RoutingInterceptionException {
    StatusbarController controller = (StatusbarController) object;
    StringBuilder sb01 = new StringBuilder();
    IStatusbarComponent component = new StatusbarComponent();
    sb01.setLength(0);
    sb01.append("component >>com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar.StatusbarComponent<< --> created using new");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    component.setController(controller);
    sb01.setLength(0);
    sb01.append("component >>").append(component.getClass().getCanonicalName()).append("<< --> created and controller instance injected");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    controller.setComponent(component);
    sb01.setLength(0);
    sb01.append("controller >>").append(controller.getClass().getCanonicalName()).append("<< --> instance of >>").append(component.getClass().getCanonicalName()).append("<< injected");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    component.render();
    sb01.setLength(0);
    sb01.append("component >>").append(component.getClass().getCanonicalName()).append("<< --> rendered");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    component.bind();
    sb01.setLength(0);
    sb01.append("component >>").append(component.getClass().getCanonicalName()).append("<< --> bound");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    ClientLogger.get().logSimple("controller >>com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar.StatusbarComponent<< created for route >>/application<<", 3);
  }

  @Override
  public void setParameter(Object object, String... params) throws RoutingInterceptionException {
  }
}
