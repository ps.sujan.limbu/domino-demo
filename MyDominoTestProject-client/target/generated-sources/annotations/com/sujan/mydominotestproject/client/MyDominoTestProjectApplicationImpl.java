package com.sujan.mydominotestproject.client;

import com.github.nalukit.nalu.client.application.IsApplicationLoader;
import com.github.nalukit.nalu.client.application.annotation.Debug;
import com.github.nalukit.nalu.client.internal.ClientLogger;
import com.github.nalukit.nalu.client.internal.application.AbstractApplication;
import com.github.nalukit.nalu.client.internal.application.ControllerFactory;
import com.github.nalukit.nalu.client.internal.application.ShellFactory;
import com.github.nalukit.nalu.client.internal.route.RouteConfig;
import com.github.nalukit.nalu.client.internal.route.ShellConfig;
import com.github.nalukit.nalu.client.plugin.IsCustomAlertPresenter;
import com.github.nalukit.nalu.client.plugin.IsCustomConfirmPresenter;
import com.github.nalukit.nalu.client.tracker.IsTracker;
import com.github.nalukit.nalu.plugin.elemental2.client.DefaultElemental2Logger;
import com.sujan.mydominotestproject.client.ui.application.popup.error.ErrorComponent;
import com.sujan.mydominotestproject.client.ui.application.popup.error.ErrorController;
import com.sujan.mydominotestproject.client.ui.application.popup.error.IErrorComponent;
import java.lang.Override;
import java.lang.StringBuilder;
import java.util.Arrays;

/**
 * Build with Nalu version >>2.0.1<< at >>2021.11.11-14:51:26<<
 */
public final class MyDominoTestProjectApplicationImpl extends AbstractApplication<MyDominoTestProjectContext> implements MyDominoTestProjectApplication {
  public MyDominoTestProjectApplicationImpl() {
    super();
    super.context = new com.sujan.mydominotestproject.client.MyDominoTestProjectContext();
  }

  @Override
  public void logProcessorVersion() {
    ClientLogger.get().logDetailed("", 0);
    ClientLogger.get().logDetailed("=================================================================================", 0);
    StringBuilder sb01 = new StringBuilder();
    sb01.append("Nalu processor version  >>2.0.1<< used to generate this source");
    ClientLogger.get().logDetailed(sb01.toString(), 0);
    ClientLogger.get().logDetailed("=================================================================================", 0);
    ClientLogger.get().logDetailed("", 0);
  }

  @Override
  public void loadDebugConfiguration() {
    ClientLogger.get().register(true, new DefaultElemental2Logger(), Debug.LogLevel.DETAILED);
  }

  @Override
  public IsTracker loadTrackerConfiguration() {
    return null;
  }

  @Override
  public void loadShells() {
    StringBuilder sb01 = new StringBuilder();
    sb01.append("load shell references");
    ClientLogger.get().logDetailed(sb01.toString(), 2);
    super.shellConfiguration.getShells().add(new ShellConfig("/application", "com.sujan.mydominotestproject.client.ui.application.shell.ApplicationShell"));
    sb01.setLength(0);
    sb01.append("register shell >>/application<< with class >>com.sujan.mydominotestproject.client.ui.application.shell.ApplicationShell<<");
    ClientLogger.get().logDetailed(sb01.toString(), 3);
  }

  @Override
  public void loadShellFactory() {
    // create ShellCreator for: com.sujan.mydominotestproject.client.ui.application.shell.ApplicationShell
    ShellFactory.get().registerShell("com.sujan.mydominotestproject.client.ui.application.shell.ApplicationShell", new com.sujan.mydominotestproject.client.ui.application.shell.ApplicationShellCreatorImpl(router, context, eventBus));
  }

  @Override
  public void loadCompositeController() {
  }

  @Override
  public void loadComponents() {
    // create ControllerCreator for: com.sujan.mydominotestproject.client.ui.application.content.screen04.Screen04Controller
    ControllerFactory.get().registerController("com.sujan.mydominotestproject.client.ui.application.content.screen04.Screen04Controller", new com.sujan.mydominotestproject.client.ui.application.content.screen04.Screen04ControllerCreatorImpl(router, context, eventBus));
    // create ControllerCreator for: com.sujan.mydominotestproject.client.ui.application.content.screen02.Screen02Controller
    ControllerFactory.get().registerController("com.sujan.mydominotestproject.client.ui.application.content.screen02.Screen02Controller", new com.sujan.mydominotestproject.client.ui.application.content.screen02.Screen02ControllerCreatorImpl(router, context, eventBus));
    // create ControllerCreator for: com.sujan.mydominotestproject.client.ui.application.shell.content.navigation.NavigationController
    ControllerFactory.get().registerController("com.sujan.mydominotestproject.client.ui.application.shell.content.navigation.NavigationController", new com.sujan.mydominotestproject.client.ui.application.shell.content.navigation.NavigationControllerCreatorImpl(router, context, eventBus));
    // create ControllerCreator for: com.sujan.mydominotestproject.client.ui.application.content.screen03.Screen03Controller
    ControllerFactory.get().registerController("com.sujan.mydominotestproject.client.ui.application.content.screen03.Screen03Controller", new com.sujan.mydominotestproject.client.ui.application.content.screen03.Screen03ControllerCreatorImpl(router, context, eventBus));
    // create ControllerCreator for: com.sujan.mydominotestproject.client.ui.application.content.screen01.Screen01Controller
    ControllerFactory.get().registerController("com.sujan.mydominotestproject.client.ui.application.content.screen01.Screen01Controller", new com.sujan.mydominotestproject.client.ui.application.content.screen01.Screen01ControllerCreatorImpl(router, context, eventBus));
    // create ControllerCreator for: com.sujan.mydominotestproject.client.ui.application.content.screen05.Screen05Controller
    ControllerFactory.get().registerController("com.sujan.mydominotestproject.client.ui.application.content.screen05.Screen05Controller", new com.sujan.mydominotestproject.client.ui.application.content.screen05.Screen05ControllerCreatorImpl(router, context, eventBus));
    // create ControllerCreator for: com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar.StatusbarController
    ControllerFactory.get().registerController("com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar.StatusbarController", new com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar.StatusbarControllerCreatorImpl(router, context, eventBus));
  }

  @Override
  public void loadRoutes() {
    StringBuilder sb01 = new StringBuilder();
    sb01.append("load routes");
    ClientLogger.get().logDetailed(sb01.toString(), 2);
    super.routerConfiguration.getRouters().add(new RouteConfig("/application/screen04", Arrays.asList(new String[]{}), "content", "com.sujan.mydominotestproject.client.ui.application.content.screen04.Screen04Controller"));
    sb01.setLength(0);
    sb01.append("register route >>/application/screen04<< with parameter >><< for selector >>content<< for controller >>com.sujan.mydominotestproject.client.ui.application.content.screen04.Screen04Controller<<");
    ClientLogger.get().logDetailed(sb01.toString(), 3);
    super.routerConfiguration.getRouters().add(new RouteConfig("/application/screen02", Arrays.asList(new String[]{}), "content", "com.sujan.mydominotestproject.client.ui.application.content.screen02.Screen02Controller"));
    sb01.setLength(0);
    sb01.append("register route >>/application/screen02<< with parameter >><< for selector >>content<< for controller >>com.sujan.mydominotestproject.client.ui.application.content.screen02.Screen02Controller<<");
    ClientLogger.get().logDetailed(sb01.toString(), 3);
    super.routerConfiguration.getRouters().add(new RouteConfig("/application", Arrays.asList(new String[]{}), "navigation", "com.sujan.mydominotestproject.client.ui.application.shell.content.navigation.NavigationController"));
    sb01.setLength(0);
    sb01.append("register route >>/application<< with parameter >><< for selector >>navigation<< for controller >>com.sujan.mydominotestproject.client.ui.application.shell.content.navigation.NavigationController<<");
    ClientLogger.get().logDetailed(sb01.toString(), 3);
    super.routerConfiguration.getRouters().add(new RouteConfig("/application/screen03", Arrays.asList(new String[]{}), "content", "com.sujan.mydominotestproject.client.ui.application.content.screen03.Screen03Controller"));
    sb01.setLength(0);
    sb01.append("register route >>/application/screen03<< with parameter >><< for selector >>content<< for controller >>com.sujan.mydominotestproject.client.ui.application.content.screen03.Screen03Controller<<");
    ClientLogger.get().logDetailed(sb01.toString(), 3);
    super.routerConfiguration.getRouters().add(new RouteConfig("/application/screen01", Arrays.asList(new String[]{}), "content", "com.sujan.mydominotestproject.client.ui.application.content.screen01.Screen01Controller"));
    sb01.setLength(0);
    sb01.append("register route >>/application/screen01<< with parameter >><< for selector >>content<< for controller >>com.sujan.mydominotestproject.client.ui.application.content.screen01.Screen01Controller<<");
    ClientLogger.get().logDetailed(sb01.toString(), 3);
    super.routerConfiguration.getRouters().add(new RouteConfig("/application/screen05", Arrays.asList(new String[]{}), "content", "com.sujan.mydominotestproject.client.ui.application.content.screen05.Screen05Controller"));
    sb01.setLength(0);
    sb01.append("register route >>/application/screen05<< with parameter >><< for selector >>content<< for controller >>com.sujan.mydominotestproject.client.ui.application.content.screen05.Screen05Controller<<");
    ClientLogger.get().logDetailed(sb01.toString(), 3);
    super.routerConfiguration.getRouters().add(new RouteConfig("/application", Arrays.asList(new String[]{}), "footer", "com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar.StatusbarController"));
    sb01.setLength(0);
    sb01.append("register route >>/application<< with parameter >><< for selector >>footer<< for controller >>com.sujan.mydominotestproject.client.ui.application.shell.content.statusbar.StatusbarController<<");
    ClientLogger.get().logDetailed(sb01.toString(), 3);
  }

  @Override
  public void loadBlockControllerFactory() {
  }

  @Override
  public void loadPopUpControllerFactory() {
  }

  @Override
  public void loadErrorPopUpController() {
    StringBuilder sb01 = new StringBuilder();
    sb01.append("ErrorPopUpController found!");
    sb01.append("create ErrorPopUpController >>com.sujan.mydominotestproject.client.ui.application.popup.error.ErrorController<<");
    ErrorController errorPopUpController = new ErrorController();
    errorPopUpController.setContext(context);
    errorPopUpController.setEventBus(eventBus);
    errorPopUpController.setRouter(router);
    sb01.setLength(0);
    sb01.append("controller >>").append(errorPopUpController.getClass().getCanonicalName()).append("<< --> created and data injected");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    IErrorComponent component = new ErrorComponent();
    sb01.setLength(0);
    sb01.append("component >>com.sujan.mydominotestproject.client.ui.application.popup.error.ErrorComponent<< --> created using new");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    component.setController(errorPopUpController);
    sb01.setLength(0);
    sb01.append("component >>").append(component.getClass().getCanonicalName()).append("<< --> created and controller instance injected");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    errorPopUpController.setComponent(component);
    sb01.setLength(0);
    sb01.append("controller >>").append(errorPopUpController.getClass().getCanonicalName()).append("<< --> instance of >>").append(component.getClass().getCanonicalName()).append("<< injected");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    component.render();
    sb01.setLength(0);
    sb01.append("component >>").append(component.getClass().getCanonicalName()).append("<< --> rendered");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    component.bind();
    sb01.setLength(0);
    sb01.append("component >>").append(component.getClass().getCanonicalName()).append("<< --> bound");
    ClientLogger.get().logDetailed(sb01.toString(), 4);
    ClientLogger.get().logSimple("controller >>com.sujan.mydominotestproject.client.ui.application.popup.error.ErrorController<< created", 3);
    errorPopUpController.onLoad();
  }

  @Override
  public void loadFilters() {
  }

  @Override
  public void loadHandlers() {
  }

  @Override
  public void loadCompositeReferences() {
    StringBuilder sb01 = new StringBuilder();
    sb01.append("load composite references");
    ClientLogger.get().logDetailed(sb01.toString(), 2);
  }

  @Override
  public void loadModules() {
  }

  @Override
  public IsApplicationLoader<MyDominoTestProjectContext> getApplicationLoader() {
    return new MyDominoTestProjectLoader();
  }

  @Override
  public IsCustomAlertPresenter getCustomAlertPresenter() {
    return null;
  }

  @Override
  public IsCustomConfirmPresenter getCustomConfirmPresenter() {
    return null;
  }

  @Override
  public void loadDefaultRoutes() {
    StringBuilder sb01 = new StringBuilder();
    this.startRoute = "/application/screen01";
    sb01.append("found startRoute >>/application/screen01<<");
    ClientLogger.get().logDetailed(sb01.toString(), 2);
    sb01.setLength(0);
    ClientLogger.get().logDetailed(sb01.toString(), 2);
  }

  @Override
  public boolean hasHistory() {
    return true;
  }

  @Override
  public boolean isUsingHash() {
    return true;
  }

  @Override
  public boolean isUsingColonForParametersInUrl() {
    return false;
  }

  @Override
  public boolean isStayOnSide() {
    return false;
  }
}
