package com.sujan.mydominotestproject.shared.model;


public enum Gender{
    MALE,
    FEMALE;
}