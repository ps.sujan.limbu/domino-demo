package com.sujan.mydominotestproject.shared.model;

public class User {
    private String id;
    private String username;
    private Gender gender;
    private String email;
    private Boolean verification;

    public User(String id, String username, Gender gender, String email, Boolean verification) {
        this.id = id;
        this.username = username;
        this.gender = gender;
        this.email = email;
        this.verification = verification;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getVerification() {
        return verification;
    }

    public void setVerification(Boolean verification) {
        this.verification = verification;
    }
}

